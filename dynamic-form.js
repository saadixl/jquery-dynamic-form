// This function adds an input under the existing input(s)
var studentInfoArray = [];

var dynamicWrapper = $("#dynamicWrapper");

$(".plus-button").click(function (e){
	e.preventDefault();
	dynamicWrapper.append('<div class="form-inline dynamiInnerDiv"><span class="glyphicon glyphicon-minus-sign minus-button marginRights"></span><input type="text" class="form-control studentNameInput marginRights" placeholder="Student Name"><input type="text" class="form-control marginRights studentEmailInput" placeholder="Student Email"><input type="checkbox" class="marginRights"/></div>');
});

// This function deletes the corresponding input
dynamicWrapper.delegate(".minus-button", "click", function (){
	$(this).parent().remove();
});

// Form submit button click event
$(".submit-button").click(function (e){
	e.preventDefault();
	setNameToInputs();
	$(".dynamiInnerDiv:not('.topLabel')").each(function (){

			var thisStudentNameInput = $(this).children("input.studentNameInput"),
				thisStudentEmailInput = $(this).children("input.studentEmailInput");
			if(inputValidityCheck(thisStudentNameInput) && inputValidityCheck(thisStudentEmailInput)){
				//$("#theDynamicForm").submit();
				alert("Form will be submitted here");
			}
			else{
				return false;
			} 

	});
	
});

// Function which sets unique name to all the dynamic inputs
function setNameToInputs(){
	var studentCount = 0;

	$(".studentNameInput").each(function (){
		studentCount++;
		var StudentName = "StudentNo"+String(studentCount);
		$(this).attr("name",StudentName);

	});
	$("#hiddenField").val(studentCount);
}

// if input is empty check

function inputValidityCheck(selector){
		 selectorVal = selector.val();
		 if(selectorVal==""){
		 	alert("Please fill up all infomartion!");
		 	selector.focus();
		 	return false;
		 }
		 else if(studentInfoArray[selectorVal]!=undefined){
		 	alert("Duplicate value!");
		 	selector.focus();
		 	return false;
		 }
		 else{
		 	studentInfoArray[selectorVal]  = 1;
		 	return true;
		 }
}